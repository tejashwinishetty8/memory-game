const game={countId:1,
result : 0,
 flipcount : 0,
 flipId2 : 0,
 flipId1 : 0,
 c : 10,
 movecount : 0,
 imagesIds : [1, 2, 3, 4, 5, 6, 7, 8],
 imagesIds1 : [101, 102, 103, 104, 105, 106, 107, 108],
 states : {},

 starcount : 0
}
let countFunction;

//constructing mainBoard
function constructmainBoard() {
  shuffleArray(game.imagesIds);
  for (var i = 0; i <= 7; i++) {
    let div = document.createElement("div");
   game.countId = game.imagesIds[i];
    div.setAttribute("id",game.countId);
    div.style.border = "3px solid #03A9F4";
    div.style.textAlign = "center";
    div.addEventListener("click", flipContent);

    maincontainer.appendChild(div);
  }

  shuffleArray(game.imagesIds1);
  for (var i = 0; i <= 7; i++) {
    let div = document.createElement("div");
   game.countId = game.imagesIds1[i];
    div.setAttribute("id",game.countId);
    div.style.border = "3px solid #03A9F4";
    div.style.textAlign = "center";
    div.addEventListener("click", flipContent);
    div.addEventListener("click", play);

    maincontainer.appendChild(div);
  }
}

function timer() {
  countFunction = setInterval(timedCount, 1000);
}


//fliping images conditions
function flipContent(e) {
  ++game.flipcount;
  game.movecount++;
  ++game.starcount;

  if (game.movecount == 1) {
    timer();
  }
  var pos = e.target;
  // if (game.states[pos.id] === "found") return;
  if (game.flipcount > 2) {
    if (pos.parentElement.id === game.flipId2) {
      return;
    }
    game.flipcount = 1;
    if (Math.abs(game.flipId2 - game.flipId1) === 100) {
      ++game.result;
      game.states[game.flipId1] = "found";
      game.states[game.flipId2] = "found";
    } else {
      game.states[game.flipId1] = "false";
      game.states[game.flipId2] = "false";
      document.getElementById(game.flipId2).childNodes[0].remove();
      document.getElementById(game.flipId1).childNodes[0].remove();
    }
  }

  if (game.flipcount === 1) {
    game.states[pos.id] === "true";
    game.flipId1 = pos.id;
    if (game.flipId1 === "") {
      game.flipcount = 0;
      return;
    }
    var img = document.createElement("img");
    img.src = "./images/" + pos.id + ".png";
    document.getElementById(pos.id).appendChild(img);
  }

  if (game.flipcount === 2) {
    if (game.result === 7) ++game.result;
    game.states[pos.id] === "true";
    game.flipId2 = pos.id;
    if (game.flipId2 === "") {
      game.flipcount = 1;
      return;
    }
    var img = document.createElement("img");
    img.src = "./images/" + pos.id + ".png";
    document.getElementById(pos.id).appendChild(img);
    alert1();
  }
  
  }
  function alert1() {
    if (game.result === 8) {
      starRating();
      delay();
      alert("you have used  "+game.starcount+"  moves     congrats u won the game");
     // window.location.reload();
    }


  if (game.movecount === 1) {
    window.addEventListener("click", play);
  }
}

function starRating(){
  var star=document.getElementsByClassName("stars_rating");
  var starratingcount=Math.floor(game.starcount/5);
  console.log(starratingcount)
  for(var i=1;i<starratingcount;i++)
  {
    console.log('entered')
    star[0].children[i].classList.toggle("checked");
  }

}

//shuffling images
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

//timer part
function timedCount() {
  label = document.getElementById("txt");
  label.innerHTML = "";
  label.appendChild(document.createTextNode(game.c));
  game.c -= 1;
  let div = document.createElement("div");

  if (game.c < 0) {
    clearInterval(countFunction);
    starRating();
    delay();
    alert("oops time out"+"     you have used  "+game.starcount+"  moves");
  // window.location.reload();
  }
}

//window loading
window.onload = function() {
  let maincontainer = document.getElementById("maincontainer");
  maincontainer.style.width = "100%";
  var button = document.getElementById("button");
  button.style.width = "60px";
  button.style.height = "20px";

  (function initial() {
    for (var i = 1; i <= 8; i++) {
      game.states[i] = "false";
    }
    for (var i = 1; i <= 8; i++) {
      game.states[100 + i] = "false";
    }
  })();

  constructmainBoard();
};

//audio part
function play() {
  var audio = document.getElementById("audio");
  audio.play();
}

function delay()
{
  setTimeout(function(){},20000);
}